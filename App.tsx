import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Home } from './src/pages/Home';
import { PinCodePage } from './src/pages/PinCodePage';
import { QRScanner } from './src/components/QRScanner';

const Stack = createNativeStackNavigator();

export const App: React.FC = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Pins" component={PinCodePage} />
        <Stack.Screen name="Scanner" component={QRScanner} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
