import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  AsyncStorage,
  Button,
} from 'react-native';

import { Pin } from '../components/Pin';
import { Separator } from '../components/Separator';

export const PinCodePage: React.FC = () => {
  const [copiedText, setCopiedText] = useState<string>('');
  const [qrCodes, setQrCodes] = useState<Object[]>([]);

  const clearAll = async () => {
    try {
      await AsyncStorage.removeItem('@MySuperStore:key');
      console.log('Success');
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const getData = async () => {
      try {
        const value = await AsyncStorage.getItem('@MySuperStore:key');
        const parsedValue = JSON.parse(value!);

        if (value !== null) {
          setQrCodes(parsedValue);
        }
      } catch (e) {
        console.log('error: ', e);
      }
    };
    getData();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View style={styles.headerContainer}>
          <Text style={styles.header}>PinCode Page</Text>
          <Separator />
        </View>
        <ScrollView>
          <View style={styles.mainContainer}>
            <Text style={styles.mainContainer_header}>
              Your recently added Pins
            </Text>
            <View>
              {qrCodes.length > 0 &&
                qrCodes.map((qrCode, index) => {
                  return (
                    <Pin
                      key={index}
                      title={qrCode.toString()}
                      setCopiedText={setCopiedText}
                    />
                  );
                })}
              {/* <FlatList
              data={data}
              renderItem={({ item }) => (
                <Pin
                  key={item.id}
                  title={item.title}
                  setCopiedText={setCopiedText}
                />
              )}
            /> */}
            </View>
          </View>
        </ScrollView>
      </View>
      {copiedText ? (
        <View style={styles.copiedText}>
          <Text>Text copied to clipoboard: {copiedText}</Text>
        </View>
      ) : (
        <></>
      )}
      <View>
        <Button title="Clear all from storage" onPress={clearAll} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  headerContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  header: {
    marginTop: 20,
    fontSize: 26,
    opacity: 0.8,
  },
  mainContainer: {
    paddingHorizontal: 30,
  },
  mainContainer_header: {
    fontSize: 18,
    marginBottom: 20,
  },
  copiedText: {
    fontSize: 18,
    alignItems: 'center',
  },
});
