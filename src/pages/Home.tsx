import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { NavigationMenu } from '../components/NavigationMenu';
import { Separator } from '../components/Separator';

import { CHECKALL } from '../images';

interface INavigation {
  navigation: any;
}

export const Home: React.FC<INavigation> = ({ navigation }) => {
  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <Text style={styles.header}>QR Code Scanner</Text>
        <Separator />
        <View style={styles.section}>
          <View style={styles.article}>
            <View style={styles.articleItem}>
              <Image style={styles.articleItem_Image} source={CHECKALL} />
              <Text style={styles.text}>
                Tap the Scan button to start scanning QR codes.
              </Text>
            </View>
            <View style={styles.articleItem}>
              <Image style={styles.articleItem_Image} source={CHECKALL} />
              <Text style={styles.text}>
                After that you will be redirected to Pins page where you will
                have to enter the Pin.
              </Text>
            </View>
          </View>
        </View>
      </View>
      <NavigationMenu navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  container: {
    backgroundColor: 'white',
    alignItems: 'center',
    flex: 1,
  },
  header: {
    marginTop: 20,
    fontSize: 26,
  },
  section: {
    width: '90%',
    elevation: 4,
    shadowOffset: { width: 10, height: 10 },
    shadowColor: 'grey',
    shadowOpacity: 0.7,
    shadowRadius: 10,
  },
  article: {
    marginTop: 30,
    padding: 20,
    borderColor: '#086EEC',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 10,
  },
  articleItem: {
    flexDirection: 'row',
    paddingRight: 20,
  },
  articleItem_Image: {
    height: 30,
    width: 30,
  },
  text: {
    fontSize: 16,
    marginBottom: 10,
  },
});
