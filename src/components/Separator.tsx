import React from 'react';
import { View, StyleSheet } from 'react-native';

export const Separator: React.FC = () => {
  return <View style={styles.separator} />;
};

const styles = StyleSheet.create({
  separator: {
    marginTop: 10,
    borderColor: 'black',
    borderWidth: 0.5,
    width: '80%',
  },
});
