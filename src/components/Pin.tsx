import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Clipboard,
} from 'react-native';

import { SHARE } from '../images';

interface IPin {
  title: string;
  setCopiedText(title: string): void;
}

export const Pin: React.FC<IPin> = ({ title, setCopiedText }) => {
  const copyToClipboard = (titleToBeCopied: string) => {
    Clipboard.setString(titleToBeCopied);
    setCopiedText(titleToBeCopied);
  };

  return (
    <TouchableOpacity
      onPress={() => {
        copyToClipboard(title);
      }}>
      <View style={styles.container}>
        <Image style={styles.containerImage} source={SHARE} />
        <Text style={styles.containerText}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
    flexDirection: 'row',
  },
  containerText: {
    fontSize: 18,
  },
  containerImage: {
    height: 22,
    width: 22,
    marginRight: 10,
  },
});
