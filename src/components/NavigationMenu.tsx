import React from 'react';
import { StyleSheet, View, Platform, Button } from 'react-native';

export const NavigationMenu = ({ navigation }: { navigation: any }) => {
  return (
    <View style={styles.navigation}>
      <View style={styles.button}>
        <Button title="Home" onPress={() => navigation.navigate('Home')} />
      </View>
      <View style={styles.button}>
        <Button title="Pins" onPress={() => navigation.navigate('Pins')} />
      </View>
      <View style={styles.button}>
        <Button title="Scan" onPress={() => navigation.navigate('Scanner')} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  navigation: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    height: Platform.OS === 'ios' ? 100 : 70,
    width: '100%',
    padding: 20,
    backgroundColor: 'white',
  },
  button: {
    borderRadius: 10,
    borderColor: '#4498ff',
    borderWidth: 2,
    padding: 5,
    backgroundColor: 'transparent',
    width: 90,
  },
});
