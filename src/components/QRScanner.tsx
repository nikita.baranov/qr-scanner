import React, { useState, useRef, useEffect } from 'react';
import { View, Alert, StyleSheet, AsyncStorage } from 'react-native';
import { RNCamera, BarCodeReadEvent } from 'react-native-camera';

interface INavigation {
  navigation: any;
}

export const QRScanner: React.FC<INavigation> = ({ navigation }) => {
  const cameraRef = useRef(null);
  const [isBarcodeRead, setIsBarcodeRead] = useState<boolean>(false);
  const [barcodeType, setBarcodeType] = useState<string>('');
  const [barcodeValue, setBarcodeValue] = useState<string>('');

  const setQrCode = async (barcode: string) => {
    try {
      let result: string | null = await AsyncStorage.getItem(
        '@MySuperStore:key',
      );
      if (result !== null) {
        let modififedArray: Array<string> = JSON.parse(result);
        modififedArray.push(barcode);
        await AsyncStorage.setItem(
          '@MySuperStore:key',
          JSON.stringify(modififedArray),
        );
      } else {
        let newArray: Array<string> = [];
        newArray.push(barcode);
        await AsyncStorage.setItem(
          '@MySuperStore:key',
          JSON.stringify(newArray),
        );
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const alertUser = (message: string): void => {
      Alert.alert(message, barcodeValue, [
        {
          text: 'OK',
          onPress: () => {
            // reset everything
            setIsBarcodeRead(false);
            setBarcodeType('');
            setBarcodeValue('');
          },
        },
      ]);
    };

    if (isBarcodeRead) {
      if (barcodeValue.length === 6) {
        setQrCode(barcodeValue);
        setTimeout(() => {
          navigation.navigate('Pins');
        }, 1000);
        alertUser('The scanned QR code is: ');
      } else {
        alertUser('The scanned QR code should consist of 6 characters');
      }
    }
  }, [isBarcodeRead, barcodeType, barcodeValue, navigation]);

  const onBarcodeRead = (e: BarCodeReadEvent): void => {
    if (!isBarcodeRead) {
      setIsBarcodeRead(true);
      setBarcodeType(e.type);
      setBarcodeValue(e.data);
    }
  };

  return (
    <View style={styles.body}>
      <RNCamera
        ref={cameraRef}
        type={RNCamera.Constants.Type.back}
        onBarCodeRead={onBarcodeRead}
        style={styles.rnCamera}
        captureAudio={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  rnCamera: {
    flex: 1,
  },
});
